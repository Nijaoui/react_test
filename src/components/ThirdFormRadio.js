import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';



/*crer le composant de from pour les questions 5 et 6*/
export class ThirdFormRadio extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (

      <Dialog
        open
        fullWidth
        maxWidth='sm'
      >
        <AppBar title="Question 5 et 6" />

        <TextField
          autoFocus
          margin="dense"
          name="q4"
          type="text"
          placeholder="Choisir la réponse"
          label="5-À quoi sert 'JSX' ?"
          onChange={handleChange('q4')}
          defaultValue={values.q4}


        />
        <br />
        <TextField
          autoFocus
          margin="dense"

          text="q4_a1"
          type="radio"
          label="à faire des appels à la base de données"
          onChange={handleChange('q4_a1')}
          defaultValue={values.q4_a1}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q4_a2"
          type="radio"
          label="à rajouter des types à Javascript"
          onChange={handleChange('q4_a2')}
          defaultValue={values.q4_a2}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q4_a3"
          type="radio"
          label="à fournir une surcouche syntaxique à Javascript pour appeler les fonctions React"
          onChange={handleChange('q4_a3')}
          defaultValue={values.q4_a3}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q4_a4"
          type="radio"
          label="c'est un autre nom pour Javascript"
          onChange={handleChange('q4_a4')}
          defaultValue={values.q4_a4}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          name="q5"
          type="text"
          placeholder="Choisir la réponse"
          label="6-Qu'est-ce qu'un 'state' ?"
          onChange={handleChange('q5')}
          value={values.q5}


        />
        <br />
        <TextField
          autoFocus
          margin="dense"

          text="q5_a1"
          type="checkbox"
          label="un stockage permanent"
          onChange={handleChange('q5_a1')}
          value={values.q5_a1}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q5_a2"
          type="checkbox"
          label="un stockage interne du composant"
          onChange={handleChange('q5_a2')}
          defaultValue={values.q5_a2}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q5_a3"
          type="checkbox"
          label="aucun des deux"
          onChange={handleChange('q5_a3')}
          defaultValue={values.q5_a3}
        />
        <br />
        <Button
          color="primary"
          variant="contained"
          onClick={this.back}
        >Back</Button>

        <Button
          color="secondary"
          variant="contained"
          onClick={this.continue}
        >Continue</Button>
      </Dialog>
    );
  }
}

export default ThirdFormRadio;