import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';



/*crer le composant de from pour les questions 3 et 4*/
export class SecondFormRadio extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (

      <Dialog
        open
        fullWidth
        maxWidth='sm'
      >
        <AppBar title="Question 3 et 4" />

        <TextField
          autoFocus
          margin="dense"
          name="q2"
          type="text"
          placeholder="Choisir la réponse"
          label="3-Qui développe React ?"
          onChange={handleChange('q2')}
          defaultValue={values.q2}


        />
        <br />
        <TextField
          autoFocus
          margin="dense"

          text="q2_a1"
          type="radio"
          label="Google"
          onChange={handleChange('q2_a1')}
          defaultValue={values.q2_a1}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q2_a2"
          type="radio"
          label="Twitter"
          onChange={handleChange('q2_a2')}
          defaultValue={values.q2_a2}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q2_a3"
          type="radio"
          label="Apple"
          onChange={handleChange('q2_a3')}
          defaultValue={values.q2_a3}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q2_a4"
          type="radio"
          label="Facebook"
          onChange={handleChange('q2_a4')}
          defaultValue={values.q2_a4}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          name="q3"
          type="text"
          placeholder="Choisir la réponse"
          label="4-À quoi servent les 'props' ?"
          onChange={handleChange('q3')}
          value={values.q3}


        />
        <br />
        <TextField
          autoFocus
          margin="dense"

          text="q3_a1"
          type="radio"
          label="à passer des propriétés au composant"
          onChange={handleChange('q3_a1')}
          value={values.q3_a1}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q3_a2"
          type="radio"
          label="à créer un état pour le composant"
          onChange={handleChange('q3_a2')}
          defaultValue={values.q3_a2}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q3_a3"
          type="radio"
          label="à afficher le composant"
          onChange={handleChange('q3_a3')}
          defaultValue={values.q3_a3}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q3_a4"
          type="radio"
          label="à casser les pieds"
          onChange={handleChange('q3_a4')}
          defaultValue={values.q3_a4}
        />
        <br />
        <Button
          color="primary"
          variant="contained"
          onClick={this.back}
        >Back</Button>

        <Button
          color="secondary"
          variant="contained"
          onClick={this.continue}
        >Continue</Button>
      </Dialog>
    );
  }
}

export default SecondFormRadio;