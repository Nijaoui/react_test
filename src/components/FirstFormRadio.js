import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

//crer le composant de from pour les questions 1 et 2
export class FirstFormRadio extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (

      <Dialog
        open
        fullWidth
        maxWidth='sm'
      >
        <AppBar title="Question 1 et  2" />

        <TextField
          autoFocus
          margin="dense"
          name="q0"
          placeholder="Choisir la réponse"
          label="1-À quoi sert React ?"
          onChange={handleChange('q0')}
          value={values.q0}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"

          text="q0_a1"
          type="radio"
          label="à administer des bases de données"
          onChange={handleChange('q0_a1')}
          value={values.q0_a1}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q0_a2"
          type="radio"
          label="à créer des interfaces utilisateurs en Javascript"
          onChange={handleChange('q0_a2')}
          value={values.q0_a2}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q0_a3"
          type="radio"
          label="à envoyer des requêtes réseau avec Javascript"
          onChange={handleChange('q0_a3')}
          value={values.q0_a3}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q0_a4"
          type="radio"
          label="à rien"
          onChange={handleChange('q0_a4')}
          value={values.q0_a3}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          name="q1"
          type="text"
          placeholder="Choisir la réponse"
          label="2-Sur quel comcept repose React ?"
          onChange={handleChange('q1')}
          value={values.q1}


        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q1_a1"
          type="radio"
          label="les modules"
          onChange={handleChange('q1_a1')}
          value={values.q1_a1}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q1_a2"
          type="radio"
          label="les services"
          onChange={handleChange('q1_a2')}
          value={values.q1_a2}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q1_a3"
          type="radio"
          label="les composants"
          onChange={handleChange('q1_a3')}
          value={values.q1_a3}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          text="q1_a4"
          type="radio"
          label="les microservices"
          onChange={handleChange('q1_a4')}
          value={values.q1_a4}
        />
        <br />
        <Button
          color="primary"
          variant="contained"
          onClick={this.back}
        >Back</Button>
        <Button
          color="secondary"
          variant="contained"
          onClick={this.continue}
        >Continue</Button>
      </Dialog>
    );
  }
}

export default FirstFormRadio;
