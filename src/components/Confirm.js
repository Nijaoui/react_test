import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import { List, ListItem, ListItemText } from '@material-ui/core/';
import Button from '@material-ui/core/Button';

//déroulement du formulaire
export class Confirm extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };
  //transferer les data 
  sendData = e =>{
    this.props = new XMLHttpRequest()
      window.addEventListener();
     e.preventDefault();
        this.props.open("POST", "http://188.165.104.163/exam");
    };
      
    
  //rendu des élèments de formulaire
      render() {
    const {
      values: { lastname, firstname,q0, q0_a1, q0_a2, q0_a3, q0_a4,q1, q1_a1, q1_a2, q1_a3, q1_a4,
        q2, q2_a1, q2_a2, q2_a3, q2_a4,q3, q3_a1, q3_a2, q3_a3, q3_a4,q4, q4_a1, q4_a2, q4_a3, q4_a4,
        q5, q5_a1, q5_a2, q5_a3,q6, q6_a1, q6_a2, q6_a3,
        q7, q7_a1, q7_a2, q7_a3,q8, q9}
    } = this.props;
    
    return (
      
          <Dialog
            open
            fullWidth
            maxWidth='sm'
          >
            <AppBar title="Question 4" />
            <List>
              <ListItem>
                <ListItemText primary="Mon nom" secondary={lastname} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Mon prénom" secondary={firstname} />
              </ListItem>
              <List>
              <ListItemText primary="1-À quoi sert React ?" secondary={q0} />
                <ListItemText primary="" secondary={q0_a1} />
                 <ListItemText primary="à créer des interfaces utilisateurs en Javascript" secondary={q0_a2} />
                <ListItemText primary="à envoyer des requêtes réseau avec Javascript" secondary={q0_a3} />
                <ListItemText primary="" secondary={q0_a4} />
                </List>
                <List>
              <ListItemText primary="2-Sur quel comcept repose React ?" secondary={q1} />
                <ListItemText primary="" secondary={q1_a1} />
                <ListItemText primary="" secondary={q1_a2} />
                <ListItemText primary="les composants" secondary={q1_a3} />
                <ListItemText primary="" secondary={q1_a4} />
                </List>
                <List>
              <ListItemText primary="3-Qui développe React ?" secondary={q2} />
                <ListItemText primary="" secondary={q2_a1} />
                <ListItemText primary="" secondary={q2_a2} />
                <ListItemText primary="" secondary={q2_a3} />
                <ListItemText primary="Facebook" secondary={q2_a4} />
                </List>
                <List>
              <ListItemText primary="4-À quoi servent les 'props' ?" secondary={q3} />
                <ListItemText primary="à passer des propriétés au composant" secondary={q3_a1} />
                <ListItemText primary="à créer un état pour le composant" secondary={q3_a2} />
                <ListItemText primary="à afficher le composant" secondary={q3_a3} />
                <ListItemText primary="" secondary={q3_a4} />
                </List>
                <List>
              <ListItemText primary="5-À quoi sert 'JSX' ?" secondary={q4} />
                <ListItemText primary="" secondary={q4_a1} />
                <ListItemText primary="à rajouter des types à Javascript" secondary={q4_a2} />
                <ListItemText primary="à fournir une surcouche syntaxique à Javascript pour appeler les fonctions React" secondary={q4_a3} />
                <ListItemText primary="" secondary={q4_a4} />
                </List>
                <List>
              <ListItemText primary="6-Qu'est-ce qu'un 'state' ?" secondary={q5} />
                <ListItemText primary="un stockage permanent" secondary={q5_a1} />
                <ListItemText primary="un stockage interne du composant" secondary={q5_a2} />
                <ListItemText primary="" secondary={q5_a3} />
                </List>
                <List>
              <ListItemText primary="7-Quels sont les avantages de React ?" secondary={q6} />
                <ListItemText primary="il permet de générer des baslises HTML dynamiquement sur le navigateur" secondary={q6_a1} />
                <ListItemText primary="il permet d'améliorer les performances en réduisant les échanges réseau" secondary={q6_a2} />
                <ListItemText primary="il permet de faire un site le plus légé possible" secondary={q6_a3} />
                </List>
                <List>
              <ListItemText primary="8-Comment puis-je faire un affichage conditionnel ?" secondary={q7} />
                <ListItemText primary="" secondary={q7_a1} />
                <ListItemText primary="En passant des paramètres à la fonction 'ReactDOM.render" secondary={q7_a2} />
                <ListItemText primary="En utilisant une expression ternaire" secondary={q7_a3} />
                </List>
                <List>
              <ListItemText primary="9-react-dom-router' permet de faire varier l'affichage en fonction de l'URL" secondary={q8} />
                <ListItemText primary="10-create-react-app' permet de générer un projet React préconfiguré" secondary={q9} />
                </List>
                </List>
            <br />
              <Button
               color="secondary"
              variant="contained"
              onClick={this.back}
            >Back</Button>

            <Button
              color="primary"
              variant="contained"
              onclick={this.sendData}
            >Confirm & Continue</Button>
          </Dialog>
          );
        }
      }

export default Confirm;
