import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


//crer le composant de from pour les questions 9 et 10

export class FifthForm extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };


  render() {
    const { values, handleChange } = this.props;
    return (

      <Dialog
        open
        fullWidth
        maxWidth='sm'
      >
        <AppBar title="Question 9 et 10 " />
        <TextField
          autoFocus
          margin="dense"
          name="q8"
          type="toggle"
          placeholder=""
          label="9-react-dom-router' permet de faire varier l'affichage en fonction de l'URL"
          onChange={handleChange('q8')}
          value={values.q8}
        />
        <br />
        <TextField
          autoFocus
          margin="dense"
          name="q9"
          type="toggle"
          label="10-create-react-app' permet de générer un projet React préconfiguré"
          onChange={handleChange('q9')}
          value={values.q9}
        />
        <br />
        <Button
          color="primary"
          variant="contained"
          onClick={this.back}
        >Back</Button>

        <Button
          color="secondary"
          variant="contained"
          onClick={this.continue}
        >Continue</Button>
      </Dialog>
    );
  }
}


export default FifthForm;