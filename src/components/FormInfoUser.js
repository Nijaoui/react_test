import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

//crer le composant de from pour les premiers infos Nom et Prenom
export class FormInfoUser extends Component {
  
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (
      
          <Dialog
            open
            fullWidth
            maxWidth='sm'
          >
            <AppBar title="Info User" />
            <TextField
              autoFocus
              margin="dense"
              name="lastname"
              type="text"
              placeholder="Enter mon lastname"
              label="1-Mon nom"
              onChange={handleChange('lastname')}
              value={values.lastname}
              
            />
            <br />
            <TextField
               autoFocus
               margin="dense"
               name="firstname"
               type="text"
               placeholder="Enter mon firstname"
               label="Mon prénom"
               onChange={handleChange('firstname')}
               value={values.firstname}
            />
            <Button
              color="secondary"
              variant="contained"
              onClick={this.continue}
            >Continue</Button>
          </Dialog>
          );
  }
}

export default FormInfoUser;
