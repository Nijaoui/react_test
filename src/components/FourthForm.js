import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';



//crer le composant de from pour les questions 7 et 8
export class FourthForm extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    };

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    };

    render() {
        const { values, handleChange } = this.props;
        return (

            <Dialog
                open
                fullWidth
                maxWidth='sm'
            >
                <AppBar title="Question 7 et 8 " />
                <TextField
                    autoFocus
                    margin="dense"
                    name="q6"
                    type="text"
                    placeholder=""
                    label="7-Quels sont les avantages de React ?"
                    onChange={handleChange('q6')}
                    value={values.q6}
                />
                <br />
                <TextField
                    autoFocus
                    margin="dense"
                    text="q6_a1"
                    type="checkbox"
                    label="il permet de générer des baslises HTML dynamiquement sur le navigateur"
                    onChange={handleChange('q6_a1')}
                    value={values.q6_a1}
                />
                <br />
                <TextField
                    autoFocus
                    margin="dense"
                    text="q6_a2"
                    type="checkbox"
                    label="il permet d'améliorer les performances en réduisant les échanges réseau"
                    onChange={handleChange('q6_a2')}
                    defaultValue={values.q6_a2}
                />
                <br />
                <TextField
                    autoFocus
                    margin="dense"
                    text="q6_a3"
                    type="checkbox"
                    label="il permet de faire un site le plus légé possible"
                    onChange={handleChange('q6_a3')}
                    defaultValue={values.q6_a3}
                />
                <br />
                <TextField
                    autoFocus
                    margin="dense"
                    name="q7"
                    placeholder=""
                    label="8-Comment puis-je faire un affichage conditionnel ?"
                    onChange={handleChange('q7')}
                    value={values.q7}
                />
                <br />
                <TextField
                    autoFocus
                    margin="dense"
                    text="q7_a1"
                    type="checkbox"
                    label="En utilisant une fonction"
                    onChange={handleChange('q7_a1')}
                    value={values.q7_a1}
                />
                <br />
                <TextField
                    autoFocus
                    margin="dense"
                    text="q7_a2"
                    type="checkbox"
                    label="En passant des paramètres à la fonction 'ReactDOM.render"
                    onChange={handleChange('q7_a2')}
                    defaultValue={values.q7_a2}
                />
                <br />
                <TextField
                    autoFocus
                    margin="dense"
                    text="q7_a3"
                    type="checkbox"
                    label="En utilisant une expression ternaire"
                    onChange={handleChange('q7_a3')}
                    defaultValue={values.q7_a3}
                />
                <br />


                <Button
                    color="primary"
                    variant="contained"
                    onClick={this.back}
                >Back</Button>

                <Button
                    color="secondary"
                    variant="contained"
                    onClick={this.continue}
                >Continue</Button>
            </Dialog>
        );
    }
}

export default FourthForm;