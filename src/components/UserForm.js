import React, { Component } from 'react';
import { FormInfoUser } from './FormInfoUser';
import { FirstFormRadio } from './FirstFormRadio';
import {SecondFormRadio} from './SecondFormRdio';
import {ThirdFormRadio} from './ThirdFormRadio';
import {FourthForm} from './FourthForm';
import {FifthForm} from './FifthForm';
import {Confirm} from './Confirm';

//
export class UserForm extends Component {
  state = {
    step: 1,
    lastname: '',
    firstname: '',
    q0: '',
    q1: '',
    q2: '',
    q3: '',
    q4: '',
    q5: '',
    q6: '',
    q7: '',
    q8: '',
    q9: ''
  };

  //stepper l'interface d'user

  // passer à l'étape suivante
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1
    });
  };

  // retourner à l'inteface précédente
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    });
  };

  // Gérer les changements de champs
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  };
  // pour poster les data à l'adresse du prof
  open = () => {
    this.props = new XMLHttpRequest();
    this.props.open('POST', 'http://188.165.104.163/exam');

  };
  

  render() {

    const { step } = this.state;
    const { lastname, firstname,q0, q1,q2,q3,q4,q5,q6,q7,q8,q9} = this.state;
    const values = { lastname, firstname,q0,q1,q2,q3,q4,q5,q6,q7,q8,q9};
  // switch pour parcourir les étapes et les composants du form
    switch (step) {
      case 1:
        return (
          <FormInfoUser
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
      case 2:
        return (
          <FirstFormRadio
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
        case 3:
        return (
          <SecondFormRadio
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
        case 4:
        return (
          <ThirdFormRadio
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
          case 5:
            return (
              <FourthForm
                nextStep={this.nextStep}
                prevStep={this.prevStep}
                handleChange={this.handleChange}
                values={values}
              />
            ); 
            case 6:
              return (
                <FifthForm
                  nextStep={this.nextStep}
                  prevStep={this.prevStep}
                  handleChange={this.handleChange}
                  values={values}
                />
              ); 
              
        case 7:
          return (
            <Confirm
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              open={this.open}
              values={values}
            />
          );
      case 8:
        return ;
      default:
        
    }
  }
}

export default UserForm;
